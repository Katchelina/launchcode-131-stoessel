package lab5;

public class Lab5Methods {

	public static int sumDownBy2(int n){
		if (n>1){
			return n+sumDownBy2(n-2);
		}
		if (n==1){
			return 1;
		}
		else{
			return 0;
		}
	}

	public static double harmonicSum(double n){
		if (n>1){
			return 1/n+harmonicSum(n-1);
		}
		else{
			return 1;
		}
	}
	//Go back to geometricSum
	public static double geometricSum(int k){
		if(k>=1){
			return 1/((double)Math.pow(2,k))+geometricSum(k-1);
		}

		else{
			return 1.0;
		}
	}

	public static int multPos(int j, int k){
		int result=0;
		for (int i=0; i<j; i++){
			result+=k;
		}
		return result;
	}

	public static int mult(int j, int k){
		//if j is negative, take absolute value of j. 
		if((j>0 && k>0) || (j<0 && k<0)){
			return multPos(Math.abs(j),Math.abs(k));
		}
		else if(j<0 || k<0){
			return -multPos(Math.abs(j),Math.abs(k));
		}
		else{
			return 0;
		}

	}

	public static int expt(int n, int k){
		int product=n;
		if(k<0){
			return 0;
		}
		else if(k==0){
			return 1;
		}
		else if (k==1){
			return n;
		}
		else{
			for(int i=1; i<k; i++){
				product= product * n;

			}
			return product;
		}
	}
	//keep
}
