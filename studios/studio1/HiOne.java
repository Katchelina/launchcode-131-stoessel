package studio1;
//In Class Exerise
//Jack: 20-3.75-5.23-4.05=6.97
//Jill: 0+5.23-1.40-2.00=3.83
//Franklin: ((0+2.00+4.05)/3)2=2.0167
//Sally: ((0+2.00+4.05)/3)=4.033
//Sum of all four results/4=$5.96
import cse131.ArgsProcessor;

public class HiOne {

	/**
	 * Says hello to the supplied argument
	 */
	public static void main(String[] args) {
		//
		// The following two lines process the input supplied when
		//    this program is run.  We don't know anything about arrays
		//    yet so you are not supposed to understand how the code
		//    works.
		// The important thing is to realize that when these two lines
		//    have done their job, the variable "name" holds the supplied
		//    input String.
		ArgsProcessor ap = new ArgsProcessor(args);
		String name = ap.nextString("What is your name?");
		//
		// Below this line, enter code so that this program's output says
		//      Hi, Pat.  How are you?
		// if the value of name is "Pat"
		//
System.out.println("Hi," + " " + name +"." + "How are you?");
	}

}
