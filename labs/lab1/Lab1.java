package lab1;

import cse131.ArgsProcessor;

public class Lab1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
	
		
	String name=ap.nextString("What is the name of the food?");	
	double carbs=ap.nextDouble("What are the grams of carbohydrates?");
	double fat=ap.nextDouble("What are the grams of fat?");
	double protein=ap.nextDouble("What are the grams of protein?");
	double statedCals=ap.nextDouble("What is the number of calories stated on the food's label?");
	//math.round((n1/n2)*100)
	//cals from carbs
	double carbCal=carbs*4;
	//cals from fat
	double fatCal=fat*9;
	//cals from protein
	double proteinCal=protein*4;
			//percent carbs
	double percentCarb=((carbCal/statedCals)*100.0);
	double newPercentCarb = Math.round(percentCarb * 10.0) /10.0;
			//percent fat
	double percentFat=((fatCal/statedCals)*100.0);
	double newPercentFat = Math.round(percentFat * 10.0) /10.0;
			//percent protein
	double percentProtein =((proteinCal/statedCals)*100.0);
	double newPercentProtein = Math.round(percentProtein * 10.0) /10.0;
	//dietary fiber
	double fiber=(carbCal+fatCal+proteinCal)-statedCals;
	double fiberUnCal=Math.round(fiber * 10.0) /10.0;
	double gramsFiber=(fiber/4);
	double newGramsFiber=Math.round(gramsFiber * 100.0) /100.0;
	//low carb diet?
boolean n1=percentCarb<=25;
	//low fat diet?
boolean n2=percentFat<=15;
	//coin flip?
double coin=Math.random();
boolean coinflip=(coin <= 0.5);
	System.out.println("This food is said to have" + " "+ statedCals + " " + "(available) Calories." +"\n"+ "With" + " " + fiberUnCal + " " + "unavailable Calories, this food has" + " " + newGramsFiber + " " + "grams of fiber" 
	+ "\n" + "\n" + "Approximately" + "\n" + "\t" + newPercentCarb + "% of your food is carbohydrates" 
			+ "\n" + "\t" + newPercentFat + "% of your food is fat" 
	+ "\n" + "\t" + newPercentProtein + "% of your food is protein"
			+ "\n" + "\n" + "This food is acceptable for a low-fat diet?:" + n2 
			+ "\n" + "This food is acceptable for a low-carb diet?:" + n1
			+ "\n" + "By coin flip, you should eat this food? " + coinflip);
	
	}

}
