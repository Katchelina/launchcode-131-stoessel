package lab4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;
import java.awt.Color;
public class BumpingBalls {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		//prompt for number of balls
		int numB=ap.nextInt("How many balls?");
		//prompt for number of iterations
		int numI=ap.nextInt("Number of iterations?");
		/*scale of coordinate system
		StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);
		 */
		double radius = 0.05; // radius
		double [] xPos= new double[numB];
		double [] yPos= new double[numB];
		double [] xVel= new double[numB];
		double [] yVel= new double[numB];
		boolean t=true;

		for (int i=0; i<numB; i++){
			//find random place to put each ball
			xPos[i] = Math.random();
			yPos[i] = Math.random();     //position
			xVel[i] = (Math.random() / 75);
			yVel[i] = (Math.random() / 75);     // velocity

		}
		while (t==true)  { 
			for(int j= 0; j<numI; j++){ 
				//clear
				StdDraw.clear();

				for(int k=0; k<numB; k++){
					if (xPos[k]> 1 || xPos[k] < 0){
						xVel[k] = -xVel[k];
					}
					if(yPos[k] > 1 || yPos[k] <0){
						yVel[k]= - yVel[k];
					}
					xPos[k] = xPos[k] + xVel[k];
					yPos[k] = yPos[k] + yVel[k];
					
					
					for(int m = 0; m<numB; m++){
						double dis = Math.sqrt(Math.pow((xPos[m]-xPos[k]), 2) + (Math.pow((yPos[m]-yPos[k]), 2)));
							if (dis <=0.06){
								xVel[k]= -xVel[k];
								yVel[k]= -yVel[k];
								xVel[m]= -xVel[m];
								yVel[m]= -yVel[m];
							}
					}
					StdDraw.setPenColor(Color.CYAN);
					StdDraw.filledCircle(xPos[k], yPos[k], 0.03);
				}
				StdDraw.show(10);

			}
			t=false; 
		}
	}
}
