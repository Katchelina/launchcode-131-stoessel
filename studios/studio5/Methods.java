package studio5;

public class Methods {
	
	/**
	 * Implemented correctly
	 * @param x one thing to add
	 * @param y the other thing to add
	 * @return the sum
	 */
	public static int sum(int x, int y) {
		return x+y;
	}
	
	/**
	 * 
	 * @param x one factor
	 * @param y another factor
	 * @return the product of the factors
	 */
	public static int mpy(int x, int y) {
		return x*y;  // FIX
	}
	
	public static double avg3(int x,  int y, int z){
		return (x+y+z)/3.0; 
	}
	
	public static double sumArray (double[] x){
		double sum = 0.0;
		for(int i=0; i<x.length; ++i){
			sum += x[i];
		}
		return sum;
	}
}
