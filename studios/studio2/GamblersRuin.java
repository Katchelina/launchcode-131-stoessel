package studio2;

import cse131.ArgsProcessor;

public class GamblersRuin {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		double startAmount = ap.nextDouble("Starting money");
		double winChance = ap.nextDouble("Win probabilty");
		double winAmount = ap.nextDouble("Win condition");
		double totalPlays = ap.nextDouble("Number of plays");
		double lossChance= 1-winChance;
		int rounds =0;
//expected ruin chance
		if (lossChance != winChance){
			double ruin = ((lossChance/winChance)*startAmount-(lossChance/winChance)*winAmount)/(1-(lossChance/winChance)*winAmount);
		} else if (lossChance == winChance){
			double ruin = 1- startAmount/winAmount;
		}
//real ruin chance
		for (int i = 1; i<=totalPlays; i++){
			double currentAmount = startAmount;
			while (currentAmount > 0 && currentAmount != winAmount){				
				double rNum = Math.random();
				if (rNum > lossChance){
					currentAmount +=1;
					rounds++;
				} else if (rNum <= lossChance){
					currentAmount -=1;
					rounds++;
				}			
			}	
			System.out.println("Simulation " + i + ": " + rounds + " rounds    "  );
		}		
	}

}
