package lab6;

import java.awt.Color;

import sedgewick.StdDraw;

public class Triangles {

	public static void draw(){
		StdDraw.setPenColor(Color.black);
		double [] xBG= {0,1,0.5};
		double [] yBG= {0,0,1};
		StdDraw.filledPolygon(xBG, yBG);

		StdDraw.setPenColor(Color.white);
		double [] xWT= {0.25, 0.50, 0.75};
		double [] yWT= {0.50, 0, 0.50};
		StdDraw.filledPolygon(xWT, yWT);
		
		StdDraw.pause(80);
		recursive(0,0,0.5);
		StdDraw.pause(80);
		recursive(0.5,0,0.5);
		StdDraw.pause(80);
		recursive(0.25,0.5,0.5);
		StdDraw.pause(80);
	}

	public static void recursive(double x, double y, double size){
		if(size<0.05){
			return;
		}
		else{
			StdDraw.setPenColor(Color.white);
			
			double [] x1= {x+size/2.0,x+size/4.0,x+size*0.75};
			double [] y1= {y, y+size/2.0, y+size/2.0};
			
			StdDraw.filledPolygon(x1, y1);
			
		
			recursive(x+size/4.0, y+size/2.0, size/2.0);
			StdDraw.pause(80);
			recursive(x,y, size/2.0);
			StdDraw.pause(80);
			recursive(x+ size/2.0, y, size/2.0);
			StdDraw.pause(80);
		
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		draw();
	}


}
