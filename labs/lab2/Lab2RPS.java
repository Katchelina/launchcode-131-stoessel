package lab2;

import cse131.ArgsProcessor;

public class Lab2RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);

		int rounds = ap.nextInt("How many rounds of 'Rock, Paper, Scissors' should be played?");
		System.out.println("You chose to have " + rounds + " rounds of RPS played.");
		double playerOneWins=0;
		double playerTwoWins=0;
		double random = Math.random();
		double draws =0;
		int playerOne;
		int playerTwo;

		for (int i = 1; i<=rounds; i++){
			System.out.println("\n" + "Round: " + i);
			if (i%3 == 0){ 
				playerOne = 1; //rock
				System.out.println("Player1 chose rock");
				playerTwo = 0;
				if (Math.random()<(1.0/3.0)){
					playerTwo = 1; //rock
					System.out.println("Player2 chose rock");
				} else if (Math.random()<(2.0/3.0)){
					playerTwo = 2; //paper
					System.out.println("Player2 chose paper");
				} else if (Math.random()<(3.0/3.0)){
					playerTwo = 0; //scissors
					System.out.println("Player2 chose scissors");
				}	
				if (playerTwo<playerOne){
					System.out.println("The winner is Player1!");
					playerOneWins++;
					
				}
				else if (playerTwo == playerOne){
					System.out.println("The round is a draw!");
					draws++;
					

				}
				else {
					System.out.println("The winner is Player2!");
				playerTwoWins++;
			}
				
			} 


			else if (i%2 == 0){ 
				playerOne = 1; //paper
				System.out.println("Player1 chose paper");
				playerTwo = 0;

				if (Math.random()<(1.0/3.0)){
					playerTwo = 0; 
					System.out.println("Player2 chose rock");//rock
				} 
				else if (Math.random()<(2.0/3.0)){
					playerTwo = 1; //paper
					System.out.println("Player2 chose paper");
				} 
				else if (Math.random()<(3.0/3.0)){
					playerTwo = 2; //scissors
					System.out.println("Player2 chose scissors");
				}	
				if (playerTwo<playerOne){
					System.out.println("The winner is Player1!");
					playerOneWins++;
					
				}
				else if (playerTwo == playerOne){
					System.out.println("The round is a draw!");
					draws++;
					
				}
				else {
					System.out.println("The winner is Player2!");
				playerTwoWins++;
				}
			
			} 


			else {	
				playerOne = 1; //scissors
				System.out.println("Player1 chose scissors");
				playerTwo = 0;
				if (Math.random()<(1.0/3.0)){
					playerTwo = 2;//rock
					System.out.println("Player2 chose rock");
				} else if (Math.random()<(2.0/3.0)){
					playerTwo = 0; //paper
					System.out.println("Player2 chose paper");
				} else if (Math.random()<(3.0/3.0)){
					playerTwo = 1; //scissors
					System.out.println("Player2 chose scissors");
				}
				if (playerTwo<playerOne){
					System.out.println("The winner is Player1!");
					playerOneWins++;
						
				}
				else if (playerTwo == playerOne){
					System.out.println("The round is a draw!");
					draws++;
					
				}
				else {
					System.out.println("The winner is Player2!");
				playerTwoWins++;
			}
				
			}



		} //for loop end

		System.out.println("\n" + "number of rounds  " +rounds);
		System.out.println("Player2 Wins  "+playerTwoWins);
		System.out.println("Player1 Wins  "+playerOneWins);
		System.out.println("draws  " + draws);

// Calculate percentages for wins and draws
		double percentPlayerOneWins= ((playerOneWins/rounds)*100.0);
		double mrPercentOne= Math.round(percentPlayerOneWins*100.0)/100.0;
		double percentPlayerTwoWins= ((playerTwoWins/rounds)*100.0);
		double mrPercentTwo= Math.round(percentPlayerTwoWins*100.0)/100.0;
		
		System.out.println("Player1 win percent:  " + mrPercentOne + "%");
		System.out.println("Player2 win percent:  " + mrPercentTwo + "%");
		// keep last two '}s'	
	}

}

