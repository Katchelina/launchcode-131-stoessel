package studio3;

import cse131.ArgsProcessor;

public class Studio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
	int x=ap.nextInt("How many numbers using Sieve of Eratosthenes?");
	
	int[] sieveOfEra=new int[x];
	System.out.println("Sieve of Eratosthenes prime numbers up to " + x);
	   for(int i=0; i<x; i++){	
		   sieveOfEra[i]=i;
	   }//end for loop1
	   
	   for(int i=2; i<x; i++){
		   
		   //if/else in second for loop
	if(sieveOfEra[i]!=0){
	for(int j=i+1; j<x; j++){
		if(sieveOfEra[j] % sieveOfEra[i]==0){
			sieveOfEra[j]=0;
		}
	}//end for loop3	
	}	   
	   }//end for loop2?
	   boolean[] primeNum=new boolean[x];
	   primeNum[0]=false;
	   primeNum[1]=false;
	   
	   for(int i=2; i<x; i++){
		   if(sieveOfEra[i] !=0){
			   primeNum[i]=true;
		   }
		   else{
			   primeNum[i]=false;
		   }
		   
	      if(sieveOfEra[i]!=0 && primeNum[i]==true){
	    	  System.out.println(sieveOfEra[i]);
	      }
	   }
	   
//leave last two }		   
	}

}
